# Sysadmin Misc.

A repository for hosting misc. sysadmin script that might be useful someday.

## `makeusers.sh`

This hackish script is used to create mass users on a GNU/Linux based OS from a single `.txt` file with usernames (one per line).  
Passwords are based on the usernames.
It takes 2 arguments:
1. Path to the users file
2. Primary group for the created users

The primary group needs to exist beforehand.
Note that this script will add users to the `docker` group, which can be a security liability. Make sure to comment this line unless you require it.
