#!/usr/bin/bash
# Usage: bash makeusers.sh newusers.txt GROUP (where newusers.txt is a text file with usernames, one per line)
# and GROUP is the primary group for these users

set -e

while read u
do
  sudo useradd -g $2 -m $u -s /bin/bash
  sudo usermod -a -G docker $u
  echo "$u:$(mkpasswd ${u:2} -m sha-512)" | sudo chpasswd -e
done < $1
